
# Using python3.6

import pandas as pd
from sklearn import linear_model
import matplotlib.pyplot as plt


# read data
dataframe = pd.read_csv('brain_body.csv')
x_values = dataframe[['Brain']]
y_values = dataframe[['Body']]

print(x_values)
print(y_values)

body_reg = linear_model.LinearRegression()
body_reg.fit(x_values, y_values)

# Visualise results
plt.scatter(x_values, y_values)
plt.plot(x_values, body_reg.predict(x_values))
plt.show()